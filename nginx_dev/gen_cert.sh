#!/bin/sh

openssl genpkey -algorithm rsa -pkeyopt rsa_keygen_bits:2048 -out private.key
openssl req -new -key private.key -out request.csr -batch
openssl x509 -in request.csr -out certificate.crt -req -signkey private.key -days 365
